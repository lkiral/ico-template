export default input =>
  input.replace(/\\u(\w\w\w\w)/g, (a, b) =>
    String.fromCharCode(parseInt(b, 16))
  );
