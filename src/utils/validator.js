const isURL = string => {
  return /^https?:\/\//.test(string);
};

const isMailTo = string => {
  return /^mailto:/.test(string);
};

export default { isURL, isMailTo };
