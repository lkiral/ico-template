module.exports = {
  /* --- navigation --- */
  'navigation.join': 'Join IEO',
  'navigation.paper': 'White Paper',
  'navigation.team': 'Team',
  'navigation.faq': 'FAQ',
  /* --- landing --- */
  'landing.title': 'J Token, Make Your eSport Dreams Come True',
  /* --- Status --- */
  'status.supply': 'Total supply',
  'status.sold': 'Token sold',
  'status.end': 'IEO ends in',
  'status.willBegin': 'IEO will begin in',
  'status.startDate': 'IEO will launch on Feb 14th, 2018. Get ready!',
  /* --- About --- */
  about: 'About  J Token',
  /* --- Applications --- */
  applications: 'Applications',
  'applications.arena.title': 'Arena',
  'applications.arena.description':
    'Professional e-sports athletes and amateur e-sports players are free to compete in the arena, a higher',
  'applications.live.title': 'Live-streaming',
  'applications.live.description':
    'In the J Token platform, gaming players can freely broadcast broadcast rhythm',
  'applications.sponsorship.title': 'Sponsorship',
  'applications.sponsorship.description':
    'Professional gaming teams and neighboring companies can compete based on player record and the popularity of the game and other data on the J Token platform to find potential newcomers',
  'applications.shopping.title': 'Shopping',
  'applications.shopping.description':
    'Gaming peripheral vendors can divide the way to invite amateur players and anchors together to sell products',
  'applications.forecast.title': 'Forecast',
  'applications.forecast.description':
    'E-sports fans can act to actually support favorite eSports teams or players and participate deeper in events',
  /* --- Road Map --- */
  roadmap: 'Roadmap',
  'roadmap.2017/08': 'Start planning J-Token platform',
  'roadmap.2017/10':
    'Invite Hong Kong-listed China Digital to be strategic partner',
  'roadmap.2017/12': 'Invite Chinese pop king - Jam Hsiao as ambassador',
  'roadmap.2018/01': 'White paper and website online',
  'roadmap.2018/02/10': 'IEO officially started',
  'roadmap.2018/03/09': 'IEO is over',
  'roadmap.2018/09': 'Platform open beta',
  'roadmap.2019/03': 'Invite famous gaming team to join J-Token platform',
  'roadmap.2019/04': 'Platform launch',
  /* --- distributions --- */
  distributions: 'IEO funds Usage',
  'distributions.development.title': 'Development',
  'distributions.development.description':
    "Utilize up to 40% of your capital to hire a highly professional and appropriate technical team to ensure that J Token's technology continues to outperform the market and continues to develop new financial products and services",
  'distributions.operation.title': 'Operation',
  'distributions.operation.description':
    'Use 20% of funds to maintain server operations and handle customer service issues',
  'distributions.marketing.title': 'Marketing',
  'distributions.marketing.description':
    'Use 20% of the funds for marketing to increase market share',
  'distributions.auditing.title': 'Auditing and Security Compliance',
  'distributions.auditing.description':
    'Use 10% of the funds for bank-level audit and safety supervision to ensure that all users of blockchain assets security',
  'distributions.legal.title': 'Legal',
  'distributions.legal.description':
    'Use 10% of the funds to legal affairs to ensure the long-term operation of J Token',
  /* --- team --- */
  team: 'Team',
  advisors: 'Advisors',
  /* --- footer ---- */
  'footer.information': 'Information',
  'footer.information.privacy': 'Privacy',
  'footer.information.terms': 'Terms and Conditions',
  'footer.support': 'Support',
  'footer.support.faq': 'FAQ',
  'footer.support.contact': 'Contact us',
  'footer.support.tutorial': 'ICO tutorial',
  'footer.social': 'Social',
  'footer.social.github': 'Github',
  'footer.social.telegram': 'Telegram',
};
