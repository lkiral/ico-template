module.exports = {
  /* --- navigation --- */
  'navigation.join': '加入 IEO',
  'navigation.paper': '白皮書',
  'navigation.team': '團隊',
  'navigation.faq': '常見問答',
  /* --- landing --- */
  'landing.title': '人人都能實現電競夢',
  /* --- Status --- */
  'status.supply': '總共供應',
  'status.sold': '已售出',
  'status.end': 'IEO 已結束',
  'status.willBegin': 'IEO 即將開始在',
  'status.startDate': 'IEO 將在 2018 二月14日開始！',
  /* --- About --- */
  about: '關於  J Token',
  /* --- Applications --- */
  applications: '應用場景',
  'applications.arena.title': '競技場',
  'applications.arena.description':
    '職業電競選手與業餘電競玩家皆可自由在競技場發起競賽，一較高下',
  'applications.live.title': '轉播平台',
  'applications.live.description':
    '在 J Token 平台，電競主播可以自由掌握播報節奏',
  'applications.sponsorship.title': '贊助平台',
  'applications.sponsorship.description':
    '職業電競戰隊及周邊廠商可以根據選手戰績與賽事人氣等數據，在J Token平台上找到潛力新星',
  'applications.shopping.title': '導購系統',
  'applications.shopping.description':
    '電競周邊廠商可以分潤方式邀請業餘玩家與主播一同銷售產品',
  'applications.forecast.title': '電競預測',
  'applications.forecast.description':
    '電競愛好者可以行動實際支持喜愛的電競戰隊或選手，更深入地參與賽事',
  /* --- Roadmap --- */
  roadmap: '路線圖',
  'roadmap.2017/08': '開始籌劃 J Token平台',
  'roadmap.2017/10': '邀請香港上市公司中國數碼成為策略夥伴',
  'roadmap.2017/12': '邀請華語流行天王——蕭敬騰成為宣傳大使',
  'roadmap.2018/01': '第一版白皮書與網站上線',
  'roadmap.2018/02/10': 'IEO正式啟動',
  'roadmap.2018/03/09': 'IEO結束',
  'roadmap.2018/09': '公開測試平台',
  'roadmap.2019/03': '邀請知名電競戰隊加盟J Token平台',
  'roadmap.2019/04': '完整系統上線',
  /* --- distributions --- */
  distributions: 'IEO 獲得資金用途',
  'distributions.development.title': '產品開發',
  'distributions.development.description':
    '運用高達 40% 之資金聘請高專業且合適之技術團隊,確保 J Token 的技術能持續領先於市場,並持續開發新金融商品與服務',
  'distributions.operation.title': '平台運營',
  'distributions.operation.description':
    '運用 20% 之資金來維繫服務器運營與處理客戶服務事項',
  'distributions.marketing.title': '市場推廣',
  'distributions.marketing.description':
    '運用 20% 之資金來進行市場推廣以提高市場佔有率',
  'distributions.auditing.title': '審計與安全監管',
  'distributions.auditing.description':
    '運用 10% 之資金進行銀行等級審計與安全監管,確保所有用戶的區塊鏈資產安全',
  'distributions.legal.title': '法務',
  'distributions.legal.description':
    '運用 10% 之資金投入法務,確保 J Token 的長期運營',
  /* --- team --- */
  team: '團隊',
  advisors: '顧問',
  /* --- footer --- */
  'footer.information': '資訊',
  'footer.information.privacy': '隱私',
  'footer.information.terms': '條款和條件',
  'footer.support': '支援',
  'footer.support.faq': '常見問答',
  'footer.support.contact': '聯繫我們',
  'footer.support.tutorial': 'ICO 教學',
  'footer.social': '社群',
  'footer.social.github': 'Github',
  'footer.social.telegram': 'Telegram',
};
