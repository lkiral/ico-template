/* Limited by gatsby-transformer-javascript-static-exports, the data only can be static and start property as `data` */

export const data = {
  language: [
    {
      langKey: 'en',
      langName: 'English',
    },
    {
      langKey: 'zh-Hant',
      langName: '中文繁體',
    },
    {
      langKey: 'zh-Han',
      langName: '中文简体',
    },
  ],
  navItems: [
    {
      id: 'join',
      to: '',
    },
    {
      id: 'paper',
      to: 'https://google.com',
    },
    {
      id: 'team',
      to: '',
    },
    {
      id: 'faq',
      to: '',
    },
  ],
  ieo: {
    startDate: '2018-02-10',
    endDate: '2018-03-09',
    hardCap: '1e10',
    totalSupply: '3e9',
    rate: '6e4',
  },
  applications: [
    { id: 'arena' },
    { id: 'live' },
    { id: 'sponsorship' },
    { id: 'shopping' },
    { id: 'forecast' },
  ],
  roadmap: [
    { year: 2017, dates: ['2017/08', '2017/10', '2017/12'] },
    { year: 2018, dates: ['2018/01', '2018/02/10', '2018/03/09', '2018/09'] },
    { year: 2019, dates: ['2019/03', '2019/04'] },
  ],
  distributions: [
    {
      id: 'development',
      ratio: 0.4,
    },
    {
      id: 'operation',
      ratio: 0.2,
    },
    {
      id: 'marketing',
      ratio: 0.2,
    },
    {
      id: 'auditing',
      ratio: 0.1,
    },
    {
      id: 'legal',
      ratio: 0.1,
    },
  ],
  team: [
    {
      firstName: 'egon',
      lastName: 'bomers',
      jobTitle: '',
      socialLinks: {
        linkedin: '',
      },
      thumbUrl: '',
      description: '',
    },
    {
      firstName: 'noah',
      lastName: 'gibson',
    },
    {
      firstName: 'justino',
      lastName: 'pires',
    },
    {
      firstName: 'milo',
      lastName: 'lefevre',
    },
    {
      firstName: 'hao',
      lastName: 'van galen',
    },
    {
      firstName: 'oscar',
      lastName: 'knubben',
    },
    {
      firstName: 'thomas',
      lastName: 'bonnet',
    },
  ],
  advisors: [
    {
      firstName: 'thomas',
      lastName: 'bonnet',
    },
  ],
  footerItems: {
    information: [
      {
        id: 'privacy',
        to: '/terms?doc=privacy',
      },
      {
        id: 'terms',
        to: '/terms',
      },
    ],
    support: [
      {
        id: 'faq',
        to: '/faq',
      },
      {
        id: 'contact',
        to: 'mailto:someone@example.com?Subject=Hello%20again',
      },
      {
        id: 'tutorial',
        to: '',
      },
    ],
    social: [
      {
        id: 'telegram',
        icon: '\ue923',
        to: '',
      },
      {
        id: 'github',
        icon: '\ue925',
        to: '',
      },
    ],
  },
};
