import React from 'react';
import moment from 'moment';
import classNames from 'classnames';

import styles from './style.module.scss';

class AnimDigit extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      update: false,
      digit: this.props.digit,
    };
  }

  componentWillReceiveProps({ digit }) {
    if (digit !== this.state.digit) {
      this.setState({
        digit,
        update: true,
      });
    }
  }

  componentDidMount() {
    this.el.addEventListener('animationend', () => {
      this.setState({ update: false });
    });
  }

  render() {
    const { digit, update } = this.state;

    return (
      <div
        className={classNames(styles.digit, { [styles.update]: update })}
        ref={c => (this.el = c)}
      >
        {digit}
      </div>
    );
  }
}

class Timer extends React.Component {
  constructor(props) {
    super(props);

    this.updateDateArr = this.updateDateArr.bind(this);

    this.state = {
      dateTimeArr: [],
    };
  }

  updateDateArr() {
    const distance = Math.floor(Math.abs(moment().diff(this.props.date) / 1e3));
    const maps = [distance, 24, 60, 60];
    const dateTimeArr = maps.map((v, idx) => {
      return (
        maps.reduce((res, a, i) => {
          return i > idx ? Math.floor(res / a) : res;
        }) % v
      );
    });

    this.setState({ dateTimeArr });
  }

  componentDidMount() {
    setInterval(this.updateDateArr, 1000);
  }

  render() {
    const labels = ['day', 'hour', 'minute', 'second'];
    const { dateTimeArr } = this.state;

    return (
      <div className={styles.timer}>
        {dateTimeArr.map((value, idx) => (
          <div className={styles.time} key={idx}>
            <div className={styles.digits}>
              {`0${value}`
                .slice(-2)
                .split('')
                .map((digit, i) => <AnimDigit key={i} digit={digit} />)}
            </div>
            <div className={styles.label}>
              {`${labels[idx]}${value === 1 ? '' : 's'}`}
            </div>
          </div>
        ))}
      </div>
    );
  }
}

export default Timer;
