import React from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { FormattedMessage } from 'react-intl';
import styles from './style.module.scss';
import Link from '../Link';

import unicode from '../../utils/unicode';

class Footer extends React.Component {
  render() {
    const footerItems = this.props.items;
    const content = Object.keys(footerItems).map(category => {
      const items = footerItems[category].map(({ to, id, icon }, idx) => (
        <Link
          className={classNames(styles.item, styles[category])}
          to={to}
          key={idx}
        >
          {icon && <span className={styles.icon}>{unicode(icon)}</span>}
          <FormattedMessage id={`footer.${category}.${id}`}>
            {text => <span className={styles.label}>{text}</span>}
          </FormattedMessage>
        </Link>
      ));

      return (
        <div className={styles['item-group']} key={category}>
          <h4 className={styles['item-title']}>
            <FormattedMessage id={`footer.${category}`} />
          </h4>
          <nav>{items}</nav>
        </div>
      );
    });

    return (
      <div className={styles.footer}>
        <div className={styles.copyright}>
          <div className={styles.logo} />
          <p>
            Copyright <br />
            2018 J Token - All Rights Reserved.
          </p>
        </div>
        <div className={styles.sitemap}>{content}</div>
      </div>
    );
  }
}

Footer.propTypes = {
  items: PropTypes.object,
};

export default Footer;
