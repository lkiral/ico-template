import React from 'react';
import classNames from 'classnames';
import moment from 'moment';
import { FormattedMessage, FormattedNumber } from 'react-intl';

import styles from './style.module.scss';
import Timer from '../Timer';

const ieoCountdown = ({ startDate, endDate }) => {
  const hasStarted = moment() > moment(startDate);
  return (
    <div className={classNames(styles.col, styles.countdown)}>
      <FormattedMessage id={`status.${hasStarted ? 'end' : 'willBegin'}`}>
        {text => <h4> {text} </h4>}
      </FormattedMessage>
      <Timer date={hasStarted ? endDate : startDate} />
      <div className={styles.message}>
        <FormattedMessage id="status.startDate">
          {text => <p> {text} </p>}
        </FormattedMessage>
      </div>
    </div>
  );
};

const ieoStatistics = ({ totalSupply, rate }) => (
  <div className={classNames(styles.col, styles.statistics)}>
    <div className={styles.info}>
      <FormattedMessage id="status.supply">
        {text => <h4> {text} </h4>}
      </FormattedMessage>
      <div className={styles.number}>
        <FormattedNumber
          value={Number(totalSupply)}
          minimumFractionDigits={0}
          style="currency"
          currency="JTK"
          currencyDisplay="name"
        >
          {amount => {
            const text = amount.split(' ');
            return <span>{`${text[0]} `}</span>;
          }}
        </FormattedNumber>
      </div>
    </div>
    <div className={styles.info}>
      <FormattedMessage id="status.sold">
        {text => <h4> {text} </h4>}
      </FormattedMessage>
      <div className={styles.number}>
        <FormattedNumber
          value={1234933}
          minimumFractionDigits={0}
          style="currency"
          currency="JTK"
          currencyDisplay="name"
        />
      </div>
    </div>
    <div className={styles.detail}>
      <div>
        1 ETH to{' '}
        <FormattedNumber
          value={Number(rate)}
          style="currency"
          currency="JTK"
          minimumFractionDigits={0}
          currencyDisplay="name"
        />
      </div>
    </div>
  </div>
);

const ieoButton = (
  <div className={classNames(styles.col, styles.button)}>
    <div className={styles['token-symbol']}>
      <div className={styles.symbol} />
      <div className={styles.text}>
        Join IEO <br />
        TODAY !
      </div>
    </div>
  </div>
);

const StatusSection = ({ ieo }) => {
  return (
    <div className={classNames('container', styles.wrapper)}>
      <div className={styles.status}>
        {ieoCountdown(ieo)}
        {ieoButton}
        {ieoStatistics(ieo)}
      </div>
    </div>
  );
};

export default StatusSection;
