import React from 'react';
import PropTypes from 'prop-types';
import Link from 'gatsby-link';

import styles from './style.module.scss';

class SelectLanguage extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      open: false,
      selected: this.props.langs.find(lang => lang.selected),
    };
  }

  render() {
    const getLangName = langKey => {
      return this.props.langMap.find(map => map.langKey === langKey).langName;
    };

    const links = this.props.langs.map(lang => (
      <Link className={styles.item} to={lang.link} key={lang.langKey}>
        <li>{getLangName(lang.langKey)}</li>
      </Link>
    ));
    return (
      <div className={styles['lang-select']}>
        <span>{getLangName(this.state.selected.langKey)}</span>
        <ul className={styles.menu}>{links}</ul>
      </div>
    );
  }
}

SelectLanguage.propTypes = {
  langs: PropTypes.array,
  langMap: PropTypes.array,
};

export default SelectLanguage;
