import React from 'react';
import classNames from 'classnames';
import { FormattedMessage } from 'react-intl';

import styles from './style.module.scss';

const ApplicationSection = props => (
  <div className="container">
    <div className={classNames(styles.application)}>
      {props.applications.map(({ id }) => (
        <div className={styles.item} key={id}>
          <FormattedMessage id={`applications.${id}.title`}>
            {text => <h3> {text} </h3>}
          </FormattedMessage>
          <FormattedMessage id={`applications.${id}.description`}>
            {text => <p> {text} </p>}
          </FormattedMessage>
        </div>
      ))}
    </div>
  </div>
);

export default ApplicationSection;
