import React from 'react';
import RouterLink from 'gatsby-link';
import validator from '../../utils/validator';

const Link = props => {
  const { to, children, ...rest } = props;
  const isURL = validator.isURL(to);
  const isMailTo = validator.isMailTo(to);
  return isURL || isMailTo ? (
    <a href={to} target={isMailTo ? '' : '_blank'} {...rest}>
      {children}
    </a>
  ) : (
    <RouterLink to={to} {...rest}>
      {children}
    </RouterLink>
  );
};

export default Link;
