import React from 'react';
import classNames from 'classnames';
import { FormattedMessage } from 'react-intl';

import styles from './style.module.scss';
import VideoPlayer from '../VideoPlayer';

const AboutSection = () => (
  <div className="container">
    <div className={classNames('container', styles.about)}>
      <FormattedMessage id="about">{text => <h2>{text}</h2>}</FormattedMessage>
    </div>
    <VideoPlayer vid="XMzM0NjMyNTAzNg" />
  </div>
);

export default AboutSection;
