import React from 'react';
import Link from '../Link';
import { FormattedMessage } from 'react-intl';
import SelectLanguage from '../SelectLanguage';
import styles from './style.module.scss';

import logo from '../../images/logo.svg';

const Header = props => {
  const navItems = props.items;
  return (
    <div className={styles.header}>
      <img src={logo} className={styles.logo} alt="j-token-logo" />
      <nav className={styles.nav}>
        {navItems.map(({ id, to }, idx) => (
          <Link className={styles['nav-item']} to={to} key={idx}>
            <FormattedMessage id={`navigation.${id}`} />
          </Link>
        ))}
        <SelectLanguage langs={props.langs} langMap={props.langMap} />
      </nav>
    </div>
  );
};

export default Header;
