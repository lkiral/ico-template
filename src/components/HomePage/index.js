import React from 'react';
import styles from './style.module.scss';

import LandingSection from '../LandingSection';
import StatusSection from '../StatusSection';
import AboutSection from '../AboutSection';
import ApplicationSection from '../ApplicationSection';
import RoadmapSection from '../RoadmapSection';
import DistributionSection from '../DistributionSection';
import TeamSection from '../TeamSection';

const HomePage = props => {
  const {
    ieo,
    distributions,
    applications,
    roadmap,
    team,
    advisors,
  } = props.data.allJsFrontmatter.edges[0].node.data;

  return (
    <main className={styles.home}>
      <div className={styles.bkg} />
      <LandingSection />
      <StatusSection ieo={ieo} />
      <AboutSection />
      <ApplicationSection applications={applications} />
      <RoadmapSection roadmap={roadmap} />
      <DistributionSection distributions={distributions} />
      <TeamSection team={team} advisors={advisors} />
    </main>
  );
};

export default HomePage;
