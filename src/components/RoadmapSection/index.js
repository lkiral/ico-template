import React from 'react';
import classNames from 'classnames';
import { FormattedMessage } from 'react-intl';

import styles from './style.module.scss';

const RoadmapSection = props => (
  <div className={classNames('container', styles.roadmap)}>
    <FormattedMessage id="roadmap">{text => <h2>{text}</h2>}</FormattedMessage>
    <div className={styles.wrapper}>
      {props.roadmap.map(yearPlan => (
        <div className={styles.section} key={yearPlan.year}>
          <div className={styles.year}>
            <h4>
              {yearPlan.year}
              <div className={styles.arrow} />
            </h4>
          </div>
          <div className={styles.items}>
            {yearPlan.dates.map((date, idx) => (
              <div className={styles.item} key={idx}>
                <span className={styles.date}> {date} </span>
                <FormattedMessage id={`roadmap.${date}`} />
              </div>
            ))}
          </div>
        </div>
      ))}
    </div>
  </div>
);

export default RoadmapSection;
