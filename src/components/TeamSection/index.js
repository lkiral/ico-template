import React from 'react';
import classNames from 'classnames';
import { FormattedMessage } from 'react-intl';

import styles from './style.module.scss';

const generateProfile = ({ firstName, lastName, description }, idx) => (
  <div className={styles.profile} key={idx}>
    <div
      className={styles.thumb}
      style={{
        backgroundImage: `url(//randomuser.me/api/portraits/med/men/${Math.floor(
          Math.random() * 100
        )}.jpg)`,
      }}
    />
    <div className={styles.info}>
      <h4 className={styles.name}> {`${firstName} ${lastName}`} </h4>
      <p className={styles.description}>{description || 'undefined'}</p>
    </div>
  </div>
);

const TeamSection = props => (
  <div className={classNames('container', styles.team)}>
    <FormattedMessage id="team">{text => <h2> {text} </h2>}</FormattedMessage>
    <div className={styles.section}>{props.team.map(generateProfile)}</div>
    <FormattedMessage id="advisors">
      {text => <h2> {text} </h2>}
    </FormattedMessage>
    <div className={styles.section}>{props.advisors.map(generateProfile)}</div>
  </div>
);

export default TeamSection;
