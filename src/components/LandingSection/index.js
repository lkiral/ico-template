import React from 'react';
import classNames from 'classnames';
import { FormattedMessage } from 'react-intl';

import styles from './style.module.scss';

const actions = [
  <div className={styles.button} key="wp">
    <a target="_blank" rel="noopener noreferrer" href="//">
      Read white paper
    </a>
  </div>,
  <div className={classNames(styles.button, styles.primary)} key="join">
    <a target="_blank" rel="noopener noreferrer" href="//">
      Join IEO
    </a>
  </div>,
];

const LandingSection = () => (
  <section className={styles.landing}>
    <h1 className={styles.title}>
      <FormattedMessage id="landing.title" />
    </h1>
    <div className={styles.actions}> {actions} </div>
  </section>
);

export default LandingSection;
