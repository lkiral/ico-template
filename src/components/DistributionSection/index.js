import React from 'react';
import { FormattedMessage } from 'react-intl';

import styles from './style.module.scss';

const DistributionSection = props => {
  const items = props.distributions.map(({ id, ratio }, idx) => {
    //const skewAngle = 90 + ratio * 180
    const skewAngle = 360 * ratio - 90;

    return (
      <div className={styles.item} key={idx}>
        <div className={styles.chart}>
          <div
            className={styles.pie}
            style={{
              transform: `skewY(${skewAngle}deg)`,
            }}
          />
          <div className={styles.label}>
            <span>{`${ratio * 100}%`}</span>
          </div>
        </div>
        <div className={styles.info}>
          <h4>
            <FormattedMessage id={`distributions.${id}.title`} />
          </h4>
          <p>
            <FormattedMessage id={`distributions.${id}.description`} />
          </p>
        </div>
      </div>
    );
  });

  return (
    <section id="distribution" className="container">
      <h2>
        <FormattedMessage id="distributions" />
      </h2>
      <div className={styles.wrapper}>{items}</div>
    </section>
  );
};

export default DistributionSection;
