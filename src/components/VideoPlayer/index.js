import React, { Component } from 'react';

// https://vthumb.ykimg.com/vi/XMzM1NjA2NDEyMA==/89/default.jpg

class VideoPlayer extends Component {
  constructor(props) {
    super(props);

    this.initPlayer = this.initPlayer.bind(this);
  }

  initPlayer() {
    const { vid } = this.props;
    const player = new window.YKU.Player(`vw-${vid}`, {
      vid,
      client_id: 'test',
    });
  }

  componentDidMount() {
    if (!window.YKU) {
      const tag = document.createElement('script');
      tag.src = '//player.youku.com/jsapi';

      const scriptTag = document.getElementsByTagName('script')[0];
      scriptTag.parentNode.insertBefore(tag, scriptTag);

      tag.addEventListener('load', () => {
        this.initPlayer();
      });
    }
  }

  render() {
    const { vid } = this.props;
    return (
      <div
        className="c-video-wrapper"
        id={`vw-${vid}`}
        ref={c => {
          this.playerEl = c;
        }}
      />
    );
  }
}

export default VideoPlayer;
