import React from 'react';
import Layout from './index';
import graphql from 'graphql';
import { addLocaleData } from 'react-intl';

import messages from '../data/messages/en';
import en from 'react-intl/locale-data/en';
import 'intl/locale-data/jsonp/en';

addLocaleData(en);

export default function LayoutWithLang(props) {
  return <Layout {...props} i18nMessages={messages} />;
}

export const pageQuery = graphql`
  query LayoutEn {
    ...languages
    ...layoutData
  }
`;
