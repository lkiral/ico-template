import React from 'react';
import PropTypes from 'prop-types';
import graphql from 'graphql';
import Helmet from 'react-helmet';
import { getCurrentLangKey, getLangs, getUrlForLang } from 'ptz-i18n';
import { IntlProvider } from 'react-intl';
import 'intl';
import 'normalize.css';
import '../styles/index.scss';

import Header from '../components/Header';
import Footer from '../components/Footer';

const TemplateWrapper = ({ children, data, location, i18nMessages }) => {
  const url = location.pathname;
  const { langs, defaultLangKey } = data.site.siteMetadata.languages;
  const {
    language,
    navItems,
    footerItems,
  } = data.allJsFrontmatter.edges[0].node.data;
  const langKey = getCurrentLangKey(langs, defaultLangKey, url);
  const homeLink = `/${langKey}/`;
  const langsMenu = getLangs(langs, langKey, getUrlForLang(homeLink, url));

  return (
    <IntlProvider locale={langKey} messages={i18nMessages}>
      <div>
        <Helmet
          title="JToken"
          meta={[
            { name: 'description', content: 'Sample' },
            { name: 'keywords', content: 'sample, something' },
          ]}
        />
        <Header langs={langsMenu} items={navItems} langMap={language} />
        <div>{children()}</div>
        <Footer items={footerItems} />
      </div>
    </IntlProvider>
  );
};

TemplateWrapper.propTypes = {
  children: PropTypes.func,
};

export default TemplateWrapper;

export const languagesFragment = graphql`
  fragment languages on RootQueryType {
    site {
      siteMetadata {
        languages {
          defaultLangKey
          langs
        }
      }
    }
    allJsFrontmatter {
      edges {
        node {
          data {
            language {
              langKey
              langName
            }
          }
        }
      }
    }
  }
`;

export const layoutFragment = graphql`
  fragment layoutData on RootQueryType {
    allJsFrontmatter {
      edges {
        node {
          data {
            navItems {
              id
              to
            }
            footerItems {
              information {
                id
                to
              }
              support {
                id
                to
              }
              social {
                id
                to
                icon
              }
            }
          }
        }
      }
    }
  }
`;

export const homeDataFragment = graphql`
  fragment homeData on RootQueryType {
    allJsFrontmatter {
      edges {
        node {
          data {
            ieo {
              startDate
              endDate
              hardCap
              totalSupply
              rate
            }
            applications {
              id
            }
            team {
              firstName
              lastName
            }
            advisors {
              firstName
              lastName
            }
            roadmap {
              year
              dates
            }
            distributions {
              id
              ratio
            }
          }
        }
      }
    }
  }
`;
