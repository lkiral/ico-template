import React from 'react';
import Layout from './index';
import graphql from 'graphql';
import { addLocaleData } from 'react-intl';

import messages from '../data/messages/zh-Hant';
import zh from 'react-intl/locale-data/zh';
import 'intl/locale-data/jsonp/zh';

addLocaleData(zh);

export default function LayoutWithLang(props) {
  return <Layout {...props} i18nMessages={messages} />;
}

export const pageQuery = graphql`
  query LayoutZh {
    ...languages
    ...layoutData
  }
`;
