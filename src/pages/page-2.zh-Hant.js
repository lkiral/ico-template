import React from 'react';
import Link from 'gatsby-link';

const SecondPage = () => (
  <div>
    <h1>中文繁體</h1>
    <p>Bem viando a pagina 2</p>
    <Link to="/zh-Hant/">Voltar pro início</Link>
  </div>
);

export default SecondPage;
