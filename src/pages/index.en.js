import React from 'react';
import graphql from 'graphql';

import HomePage from '../components/HomePage';

const IndexPage = props => {
  return <HomePage {...props} />;
};

export default IndexPage;

export const pageQuery = graphql`
  query HomeEn {
    ...homeData
  }
`;
