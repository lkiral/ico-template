module.exports = {
  parserOptions: {
    sourceType: "module",
    ecmaVersion: 2017,
    ecmaFeatures: {
      jsx: true,
      modules: true,
      experimentalObjectRestSpread: true
    }
  },
  extends: [
    "eslint:recommended",
    "plugin:import/errors",
    "plugin:import/warnings",
    "plugin:react/recommended",
    "prettier",
    "prettier/react",
    "prettier/standard"
  ],
  plugins: [
    "react",
    "import",
    "prettier",
  ],
  env: {
    browser: true,
    node: true,
    es6: true,
    jest: true
  },
  rules: {
    'prettier/prettier': ["error", {
      "trailingComma": "es5",
      "singleQuote": true,
    }],
    'no-console': ['error', { allow: ['warn', 'error'] }], // no console.log, but can use .warn and .error.
    'react/prop-types': 'off', // we don't use propTypes.
  }
}
