const autoprefixer = require('autoprefixer');
const languages = require('./src/data/languages');

module.exports = {
  siteMetadata: {
    title: 'Jtoken: Build for Gaming ECO-system',
    languages,
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-react-next',
    {
      resolve: 'gatsby-plugin-postcss-sass',
      options: {
        postCssPlugins: [
          autoprefixer({
            browsers: ['last 2 versions'],
          }),
        ],
        precision: 8,
      },
    },
    {
      resolve: 'gatsby-plugin-i18n',
      options: {
        langKeyForNull: 'any',
        langKeyDefault: languages.defaultLangKey,
        useLangKeyLayout: true,
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'content',
        path: `${__dirname}/src/data/content/`,
      },
    },
    'gatsby-transformer-javascript-static-exports',
    'gatsby-transformer-json',
  ],
};
