# gatsby ico template

```bash
# develop
npm start

# lint
npm run lint
npm run lint:fix

# clean 
npm run clean
npm run clean:cache

# build
npm run build
```
